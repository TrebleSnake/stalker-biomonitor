var ResultBuilder = {
    invalid: function () {
        return '<b>Ошибка!</b> Неверные параметры для запуска!'
    },

    build: function () {
        if (AlchemyApp.state.result == AlchemyApp.RESULTS.NOTHING)
            return '<b>Неудача!</b> Не было зарегистрировано изменений у субъекта в результате эксперимента.';

        var prefix = '<b>Успех!</b> Удалось зарегистрировать изменения у субъекта после эксперимента. Полученные эффекты:<br/>';
        return prefix + this._getResultText();
    },

    _getResultText: function () {
        var text = [];
        var positive = undefined, negative = undefined;

        // выбираем положительный эффект
        if (AlchemyApp.state.result == AlchemyApp.RESULTS.BOTH || AlchemyApp.state.result == AlchemyApp.RESULTS.POSITIVE) {
            positive = AlchemyApp.chooseChance(this.effectsChance.positive);
            text.push(this.effectsNames.positive[positive]);
        }

        // выбираем отрицательный эффект
        if (AlchemyApp.state.result == AlchemyApp.RESULTS.BOTH || AlchemyApp.state.result == AlchemyApp.RESULTS.NEGATIVE) {
            var effects = this.effectsChance.negative;

            // если выпало два эффекта, исключаем смерть
            if(AlchemyApp.state.result == AlchemyApp.RESULTS.BOTH)
                effects = _.omit(effects, 'death');

            // если у нас есть положительный эффект, исключаем его "зеркало"
            if(!_.isUndefined(positive))
                effects = _.omit(effects, positive);

            negative = AlchemyApp.chooseChance(effects);

            // если ине удалось определить эффект, выбираем наиболее вероятный
            if(_.isUndefined(negative)) {
                var tmp = 0;
                _.each(_.keys(effects), function(key){
                    if(effects[key] > tmp) {
                        tmp = effects[key];
                        negative = key
                    }
                })
            }

            text.push(this.effectsNames.negative[negative]);
        }
        return text.join('<br/>')
    },

    effectsChance: {
        positive: {
            'reload': 0.2,
            'x2hp': 0.1,
            'hp': 0.05,
            'shock': 0.2,
            'invul': 0.2,
            'regen': 0.25

        },
        negative: {
            'reload': 0.35,
            'hp': 0.15,
            'shock': 0.2,
            'invul': 0.2,
            'death': 0.1

        }
    },
    effectsNames: {
        positive: {
            'reload': 'Ускоренные рефлексы (перезарядка ускоряется на 1/2/3 секунды)',
            'x2hp': 'Буря адреналина (х2 ХП однократно)',
            'hp': 'Каменная кожа (+20 к здоровью)',
            'shock': 'Повышенный болевой порог (-0.5 сек ко времени шока)',
            'invul': 'Хамелеон (+0.5 сек ко времени неуязвимости)',
            'regen': 'Спонтанная регенерация (восстановление тяжелого ранения)'

        },
        negative: {
            'reload': 'Потеря мышечной памяти (перезарядка замедляется на 1/2/3 секунды)',
            'hp': 'Несвертываемость крови (-10 к здоровью)',
            'shock': 'Понижен болевой порог (+0.5 сек ко времени шока)',
            'invul': 'Спонтанный паралич (-0.5 сек ко времени неуязвимости)',
            'death': 'Смерть (сорян ☹)'

        }
    }
};