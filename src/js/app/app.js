var AlchemyApp = {
    state: {
        params: undefined,
        step: 0,
        valid: false,
        result: undefined,
        liquid: undefined,
        isFinalPhase: function() { return this.step >= AlchemyApp.config.steps * 0.75 }
    },

    config: {
        interval: 500,
        steps: 9999,
        renderers: []
    },

    RESULTS: {
        NOTHING: '0',
        BOTH: '1',
        POSITIVE: '2',
        NEGATIVE: '3'
    },

    initialize: function () {
        this.initChanses();
        this.liquid = LiquidRenderer();
        this.config.renderers.push(this.liquid);
        var self = this;
        _.each(this.config.renderers, function(item) {
            item.initialize(self.config)
        });
        $('#heartMonitor').hide()
    },

    reset: function () {
        this.state.step = 0;
        this.state.params = undefined;
        this.state.valid = false;
        this.state.result = undefined;
        var self = this;
        _.each(this.config.renderers, function (item) {
            item.reset(self.config)
        })
        $('#heartMonitor').hide()
        $('#run').removeClass('disabled');
    },

    run: function (params) {
        if (!_.isUndefined(params)) {
            if (_.isUndefined(this.CHANCES[params.expStage])) {
                $('#results').modal({show: true, keyboard: true});
                return
            }

            $('#heartMonitor').show();
            $('#run').addClass('disabled');
            this.state.params = params;
            this.state.result = this.getResult();
            if (_.isUndefined(this.state.result)) {
                $('#results').modal({show: true, keyboard: true});
                return
            }

            this.state.valid = true;

            // debug
            //console.log(params);
            //console.info('res = ' + this.state.result);
            //this.state.step = this.config.steps;
            // ---

            this.setTimeout();
        }
    },

    getResult: function () {
        var params = this.state.params;

        // статичные результаты
        if (params.subjectCnd == 'bad')
            return this.RESULTS.NEGATIVE;
        if (params.subjectCnd == 'medium')
            return this.RESULTS.BOTH;
        if (params.subjectCnd == 'nice')
            return this.RESULTS.POSITIVE;

        // динамические результаты
        return this.chooseChance(this.CHANCES[params.expStage]);

    },

    chooseChance: function (chances) {
        var max = 100;
        var n = Math.random() * max;
        var tmp = 0;

        return _.find(_.keys(chances), function (key) {
            var chance = chances[key];
            var d = chance * max;
            if (tmp <= n && n < tmp + d)
                return true;

            tmp += d;
            return false;
        })
    },

    setTimeout: function () {
        setTimeout(function () {
            AlchemyApp.tic()
        }, this.config.interval)
    },

    tic: function () {
        var self = this;
        _.each(this.config.renderers, function(item) {
            item.render(self.state, self.config)
        });

        this.state.step += 1;
        if (this.state.step < this.config.steps)
            this.setTimeout();
        else
            $('#results').modal({show: true, keyboard: true});
    },

    CHANCES: {},
    initChanses: function () {
        var chances = this.CHANCES;
        var results = this.RESULTS;
        var stage;

        stage = {};
        stage[results.NOTHING] = 0.15;
        stage[results.BOTH] = 0.1;
        stage[results.NEGATIVE] = 0.65;
        stage[results.POSITIVE] = 0.1;
        chances['1'] = stage;

        stage = {};
        stage[results.NOTHING] = 0.1;
        stage[results.BOTH] = 0.2;
        stage[results.NEGATIVE] = 0.5;
        stage[results.POSITIVE] = 0.2;
        chances['2'] = stage;

        stage = {};
        stage[results.NOTHING] = 0.1;
        stage[results.BOTH] = 0.3;
        stage[results.NEGATIVE] = 0.4;
        stage[results.POSITIVE] = 0.2;
        chances['3'] = stage;

        stage = {};
        stage[results.NOTHING] = 0.1;
        stage[results.BOTH] = 0.4;
        stage[results.NEGATIVE] = 0.25;
        stage[results.POSITIVE] = 0.25;
        chances['4'] = stage;

        stage = {};
        stage[results.NOTHING] = 0.05;
        stage[results.BOTH] = 0.50;
        stage[results.NEGATIVE] = 0.15;
        stage[results.POSITIVE] = 0.30;
        chances['5'] = stage;
    }
};