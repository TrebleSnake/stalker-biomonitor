$(document).ready(function(){

    var app = AlchemyApp;
    app.initialize();

    $("#run").click(function () {
        app.run({
            blood: $("#blood").find(".active input").val(),
            subjectCnd: $("#subjectCnd").find(".active input").val(),
            expStage: $("#expStage").find(".active input").val()
        })
    });

    $('#results').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('#resultsText')
            .removeClass('alert-danger')
            .removeClass('alert-warning')
            .removeClass('alert-success');

        if(!AlchemyApp.state.valid) {
            modal.find('#resultsText').addClass('alert-warning')
                .html(ResultBuilder.invalid());
            AlchemyApp.reset();
            return;
        }

        modal.find('#resultsText').addClass(getCssClass(app.state.result))
            .html(ResultBuilder.build());
        AlchemyApp.reset();
    });

    function getCssClass(result) {
        if (result == app.RESULTS.POSITIVE)
            return 'alert-success';
        if (result == app.RESULTS.BOTH)
            return 'alert-warning';

        return 'alert-danger'
    }
});
